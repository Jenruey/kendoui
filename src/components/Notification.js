import { Slide } from "@progress/kendo-react-animation";
import {
  Notification,
  NotificationGroup
} from "@progress/kendo-react-notification";
import * as React from "react";
import CustomEvents from "../constants/CustomEvents";

function Notifications() {
  const [data, setData] = React.useState(null);

  const sendNotif = ({ detail }) => {
    if (detail && detail.type && detail.message) {
      setData(detail);
    } else {
      setData({ type: "error", message: "Notification error." });
    }
    setTimeout(() => {
      setData(null);
    }, 3000);
  };

  React.useEffect(() => {
    window.addEventListener(CustomEvents.sendNotification, sendNotif);
    return function unmount() {
      window.removeEventListener(CustomEvents.sendNotification, sendNotif);
    };
  });

  return (
    <NotificationGroup
      style={{
        right: 0,
        bottom: 0,
        alignItems: "flex-start",
        flexWrap: "wrap-reverse",
        zIndex: 1200
      }}
    >
      <Slide enter={true} exit={true} direction={"left"}>
        {data && (
          <Notification
            type={{ style: data.type, icon: true }}
            closable={true}
            onClose={() => setData(null)}
          >
            <span>{data.message}</span>
          </Notification>
        )}
      </Slide>
    </NotificationGroup>
  );
}

export default Notifications;

// sendNotification({ type: "success", message: "hello" });
export function sendNotification(data) {
  window.dispatchEvent(
    new CustomEvent(CustomEvents.sendNotification, {
      detail: data
    })
  );
}
