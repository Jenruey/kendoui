import { BrowserRouter, Route, Switch } from "react-router-dom";
import Notifications from "./components/Notification";
import MainBar from "./navigation/MainBar";
import NavBar from "./navigation/NavBar";
import PathLink from "./navigation/PathLink";
import Main from "./pages/inner/Main";
import Home from "./pages/main/Home";
import Login from "./pages/main/Login";

function App() {
  return (
    <BrowserRouter>
      {/* Website notifcation setup */}
      <Notifications />
      {/* Modal switcher (those modal with url)*/}
      <Switch>
        <Route path={PathLink.login}>
          <Login />
        </Route>
      </Switch>
      {/* Page switcher */}
      <Switch>
        <Route path={PathLink.main}>
          <MainBar>
            <Main />
          </MainBar>
        </Route>
        <Route>
          <NavBar />
          <Home />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
