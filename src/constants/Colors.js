const Colors = {
  mainBgColor: "#edf3f6",
  linkHoverColor: "#84cef1"
};

export default Colors;
