import { Button } from "@progress/kendo-react-buttons";
import { Dialog, DialogActionsBar } from "@progress/kendo-react-dialogs";
import { Field, Form } from "@progress/kendo-react-form";
import { useInternationalization } from "@progress/kendo-react-intl";
import { Popup } from "@progress/kendo-react-popup";
import {
  AgendaView,
  DayView,
  MonthView,
  Scheduler,
  SchedulerEditItem,
  SchedulerEditSlot,
  SchedulerItem,
  SchedulerItemContent,
  TimelineView,
  WeekView
} from "@progress/kendo-react-scheduler";
import { DropDownList } from "@progress/kendo-react-scheduler/dist/npm/editors/DropDownList";
import { MultiSelect } from "@progress/kendo-react-scheduler/dist/npm/editors/MultiSelect";
import { SchedulerEditor } from "@progress/kendo-react-scheduler/dist/npm/editors/SchedulerEditor";
import _ from "lodash";
import React from "react";
import ReactDOM from "react-dom";

const baseData = [
  {
    TaskID: 1,
    Title: "Bowling tournament",
    Description: "",
    StartTimezone: null,
    // reminder: when storing to databse use moment.format()
    Start: "2020-12-09T21:00:00+08:00",
    End: "2020-12-10T00:00:00+08:00",
    EndTimezone: null,
    RecurrenceRule: "FREQ=DAILY;BYDAY=TU,TH;UNTIL=2021-01-01T00:00:00+08:00",
    RecurrenceID: null,
    RecurrenceException: null,
    isAllDay: false,

    TeamID: 2,
    OwnerID: 1,
    image:
      "https://d585tldpucybw.cloudfront.net/sfimages/default-source/default-album/release_webinar_kui_1920x440_lp.png?sfvrsn=23989f80_0"
  }
];

const currentYear = new Date().getFullYear();
const parseAdjust = (eventDate) => {
  const date = new Date(eventDate);
  date.setFullYear(currentYear);
  return date;
};

const sampleData = baseData.map((dataItem) => ({
  // these are required fields..
  id: dataItem.TaskID,
  start: parseAdjust(dataItem.Start),
  startTimezone: dataItem.startTimezone,
  end: parseAdjust(dataItem.End),
  endTimezone: dataItem.endTimezone,
  isAllDay: dataItem.isAllDay,
  title: dataItem.Title,
  description: dataItem.Description,
  recurrenceRule: dataItem.RecurrenceRule,
  recurrenceId: dataItem.RecurrenceID,
  recurrenceExceptions: dataItem.RecurrenceException,

  // these are custom field for our use..
  teamId: dataItem.TeamID,
  personId: dataItem.OwnerID,
  image: dataItem.image
}));

const resources = [
  {
    name: "Teams",
    data: [
      { text: "Team Nugget", value: 1, color: "red" },
      { text: "Team Chicken", value: 2, color: "yellow" }
    ],
    field: "teamId",
    valueField: "value",
    textField: "text",
    colorField: "color"
  },
  {
    name: "Persons",
    data: [
      { text: "Peter", value: 1, color: "green" },
      { text: "Alex", value: 2, color: "pink" }
    ],
    field: "personId",
    valueField: "value",
    textField: "text",
    colorField: "color"
  }
];

const CustomItem = (props) => {
  const ref = React.useRef(null);
  const intl = useInternationalization();
  const [show, setShow] = React.useState(false);

  const handleFocus = React.useCallback(
    (event) => {
      setShow(true);

      // Call the default `onFocus` handler
      if (props.onFocus) {
        props.onFocus(event);
      }
    },
    [setShow, props.onFocus] // eslint-disable-line react-hooks/exhaustive-deps
  );
  const handleBlur = React.useCallback(
    (event) => {
      setShow(false);

      // Call the default `onBlur` handler
      if (props.onBlur) {
        props.onBlur(event);
      }
    },
    [setShow, props.onBlur] // eslint-disable-line react-hooks/exhaustive-deps
  );

  return (
    <SchedulerItem
      {...props}
      ref={ref}
      onFocus={handleFocus}
      onBlur={handleBlur}
    >
      {props.children}
      {!props.isAllDay && (
        <SchedulerItemContent>
          {intl.formatDate(props.zonedStart, "t") +
            " - " +
            intl.formatDate(props.zonedEnd, "t")}
        </SchedulerItemContent>
      )}
      <Popup
        show={show}
        // point of anchor
        anchorAlign={{
          horizontal: "center",
          vertical: "top"
        }}
        // point of popup
        popupAlign={{
          horizontal: "center",
          vertical: "bottom"
        }}
        anchor={ref.current && ref.current.element}
      >
        <div className="rounded" style={{ overflow: "hidden" }}>
          <img
            alt={"event-background"}
            className="image-fluid"
            src={props.dataItem.image}
            style={{ width: 400, height: "auto" }}
          />
          <div className="p-1">
            <h5>{props.title}</h5>
            <div>Start: {intl.formatDate(props.zonedStart, "t")}</div>
            <div>End: {intl.formatDate(props.zonedEnd, "t")}</div>
          </div>
        </div>
      </Popup>
    </SchedulerItem>
  );
};

const testingRemoveDialog = (props) => {
  return ReactDOM.createPortal(
    <Dialog>
      <DialogActionsBar>
        <Button onClick={props.onClose}>close</Button>
        <Button onClick={props.onCancel}>cancel</Button>
        <Button onClick={props.onConfirm}>confirm</Button>
      </DialogActionsBar>
    </Dialog>,
    document && document.body
  );
};

const testingOccurrenceDialog = (props) => {
  let { isRemove, onClose, onSeriesClick, onOccurrenceClick } = props;
  let text = isRemove ? "remove" : "edit";
  return ReactDOM.createPortal(
    <Dialog>
      <DialogActionsBar>
        <Button onClick={onClose}>close</Button>
        <Button onClick={onSeriesClick}>{text + " series"}</Button>
        <Button onClick={onOccurrenceClick}>{text + " occurrence"}</Button>
      </DialogActionsBar>
    </Dialog>,
    document && document.body
  );
};

var generateResourceEditors = function (_resources) {
  if (!_resources) {
    return null;
  }
  return _resources.map(function (resource) {
    return React.createElement(
      "div",
      { key: resource.field },
      React.createElement(
        "div",
        { className: "k-edit-label" },
        React.createElement("label", null, resource.name)
      ),
      React.createElement(
        "div",
        { className: "k-edit-field" },
        resource.multiple &&
          React.createElement(Field, {
            name: resource.field,
            component: MultiSelect,
            data: resource.data,
            textField: resource.textField,
            dataItemKey: resource.valueField,
            colorField: resource.colorField
          }),
        !resource.multiple &&
          React.createElement(Field, {
            name: resource.field,
            component: DropDownList,
            data: resource.data,
            textField: resource.textField,
            dataItemKey: resource.valueField,
            colorField: resource.colorField
          })
      )
    );
  });
};

const TestingSchedulerForm = (props) => {
  let { onCancel, onClose, onSubmit } = props;
  var handleSubmit = React.useCallback(
    function (dataItem, syntheticEvent) {
      if (onSubmit) {
        onSubmit.call(undefined, {
          value: dataItem,
          syntheticEvent: syntheticEvent
        });
      }
    },
    [onSubmit]
  );

  return ReactDOM.createPortal(
    <Form
      initialValues={props.dataItem}
      onSubmit={handleSubmit}
      render={(renderProps) => (
        <Dialog
          title={"Title"}
          minWidth={600}
          onClose={onClose}
          className={"k-scheduler-edit-dialog"}
          style={{ zIndex: 90 }}
        >
          <SchedulerEditor {...renderProps} />
          {generateResourceEditors(resources)}
          <DialogActionsBar>
            <Button
              disabled={!renderProps.allowSubmit}
              onClick={renderProps.onSubmit}
            >
              Save
            </Button>
            <Button onClick={onCancel}>Cancel</Button>
          </DialogActionsBar>
        </Dialog>
      )}
    />,
    document && document.body
  );
};

const CustomEditItem = (props) => {
  return (
    <SchedulerEditItem
      {...props}
      //  props.onCancel, props.onClose, props.onSubmit;
      form={TestingSchedulerForm} // SchedulerForm}
      // props.isRemove (boolean), props.onClose, props.onSeriesClick, props.onOccurrenceClick;
      occurrenceDialog={testingOccurrenceDialog} // SchedulerOccurrenceDialog}
      // props.onClose,  props.onCancel,  props.onConfirm;
      removeDialog={testingRemoveDialog} // SchedulerRemoveDialog}
    />
  );
};

const testingSchedulerEdit = (props) => {
  return <SchedulerEditSlot {...props} form={TestingSchedulerForm} />;
};

function Calendar() {
  const [scData, setScData] = React.useState(sampleData);

  const onDataChange = (data) => {
    console.log(data);
    let nextScData = _.cloneDeep(scData);

    if (data.created.length > 0) {
      let lastId = 1;
      if (nextScData.length > 0) {
        lastId = nextScData[nextScData.length - 1].id + 1;
      }
      for (let newEvent of data.created) {
        nextScData.push({ ...newEvent, id: lastId });
        lastId++;
      }
    }

    if (data.deleted.length > 0) {
      for (let delEvent of data.deleted) {
        let delIndex = _.findIndex(nextScData, function (o) {
          return o.id === delEvent.id;
        });
        nextScData.splice(delIndex, 1);
      }
    }

    if (data.updated.length > 0) {
      for (let upEvent of data.updated) {
        let upIndex = _.findIndex(nextScData, function (o) {
          return o.id === upEvent.id;
        });
        nextScData[upIndex] = upEvent;
      }
    }

    nextScData = _.sortBy(nextScData, [
      function (o) {
        return o.id;
      }
    ]);
    setScData(nextScData);
  };

  return (
    <div>
      <hl>Calendar</hl>
      <Scheduler
        resources={resources}
        data={scData}
        onDataChange={onDataChange}
        editable={{
          add: true,
          remove: true,
          drag: true,
          resize: true,
          edit: true
        }}
        item={CustomItem}
        editItem={CustomEditItem}
        editSlot={testingSchedulerEdit}
      >
        <MonthView />
        <TimelineView />
        <DayView />
        <WeekView />
        <AgendaView />
      </Scheduler>
    </div>
  );
}

export default Calendar;
