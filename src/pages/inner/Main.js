import { Route, Switch } from "react-router-dom";
import PathLink from "../../navigation/PathLink";
import Calendar from "./Calendar";

function Main() {
  return (
    <Switch>
      <Route path={PathLink.calendar}>
        <Calendar />
      </Route>
    </Switch>
  );
}

export default Main;
