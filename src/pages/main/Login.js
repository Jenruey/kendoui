import { Button } from "@progress/kendo-react-buttons";
import { Icon } from "@progress/kendo-react-common";
import { Field, Form, FormElement } from "@progress/kendo-react-form";
import React from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import { ValidatedInput } from "../../components/form-assist/FormItem";
import { emailRegex } from "../../components/form-assist/RegularExpression";
import { sendNotification } from "../../components/Notification";
import PathLink from "../../navigation/PathLink";

const AbosContainer = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 1000;
  display: flex;
  align-items: center;
  justify-content: center;
`;

function Login() {
  const history = useHistory();

  // const FormValidator = (values) => {
  //   console.log("testing1");
  //   const email = getter("login.email")(values);
  //   const password = getter("login.password")(values);
  //   console.log("testing2");
  //   if (email && password) return {};
  //   console.log("testing3");
  //   return {
  //     VALIDATION_SUMMARY: "Please fill the required fields.",
  //     ["login.email"]: "This is required field.",
  //     ["login.password"]: "This is required field."
  //   };
  // };

  return (
    <AbosContainer>
      <div
        style={{
          backgroundColor: "white",
          width: "90%",
          maxWidth: "400px",
          padding: "20px 40px"
        }}
      >
        <div className={"text-right mb-3"}>
          <Button
            look={"clear"}
            style={{ padding: 0 }}
            onClick={() => {
              history.goBack();
            }}
          >
            <Icon name={"close"} />
          </Button>
        </div>
        <Form
          onSubmit={() => {
            sendNotification({
              type: "success",
              message: "Congratulation, login successfully."
            });
            history.push(PathLink.main);
          }}
          render={(formRenderProps) => (
            <FormElement>
              <fieldset className={"k-form-fieldset"}>
                <legend className={"k-form-legend"}>Get started today!</legend>
                {formRenderProps.visited &&
                  formRenderProps.errors &&
                  formRenderProps.errors.VALIDATION_SUMMARY && (
                    <div className={"k-messagebox k-messagebox-error"}>
                      {formRenderProps.errors.VALIDATION_SUMMARY}
                    </div>
                  )}
                <div className={"mb-3"}>
                  <Field
                    name={"login.email"}
                    type={"email"}
                    component={ValidatedInput}
                    label={"Email"}
                    validator={function (value) {
                      return emailRegex.test(value)
                        ? ""
                        : "Please enter a valid email.";
                    }}
                  />
                </div>
                <div className={"mb-3"}>
                  <Field
                    name={"login.password"}
                    type={"password"}
                    component={ValidatedInput}
                    label={"Password"}
                    validator={function (value) {
                      return value ? "" : "This is required field.";
                    }}
                  />
                </div>
              </fieldset>
              <div className={"k-form-buttons justify-content-center"}>
                <Button
                  type={"submit"}
                  className="k-button"
                  disabled={!formRenderProps.allowSubmit}
                >
                  Submit
                </Button>
              </div>
            </FormElement>
          )}
        />
      </div>
    </AbosContainer>
  );
}

export default Login;
