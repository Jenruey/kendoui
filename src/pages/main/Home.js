import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import styled from "styled-components";
import ProfileOneBg from "../../asset/images/home--hero-background.png";

const ProfileOne = styled(Col)`
  background-image: url(${ProfileOneBg});
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  height: calc(100vh - 95px);
  min-height: 600px;
`;

function Home() {
  return (
    <Container fluid>
      <Row>
        <ProfileOne xl={12} />
        <Col xl={12} style={{ backgroundColor: "white", height: "1000px" }} />
      </Row>
    </Container>
  );
}

export default Home;
