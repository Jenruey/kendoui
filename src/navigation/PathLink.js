const PathLink = {
  home: "/",
  login: "/login",

  //after login, default url will be /main/...
  main: "/main",
  calendar: "/main/calendar"
};

export default PathLink;
