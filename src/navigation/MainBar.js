import {
  AppBar,
  AppBarSection,
  AppBarSpacer
} from "@progress/kendo-react-layout";
import { useHistory } from "react-router-dom";
import PathLink from "./PathLink";

function MainBar(props) {
  const history = useHistory();
  return (
    <>
      <AppBar id={"App-header"} positionMode={"fixed"}>
        <AppBarSection>
          <h1>Kendo</h1>
        </AppBarSection>
        <AppBarSection>
          {[
            { page: "Calendar", path: PathLink.calendar },
            { page: "Logout", path: PathLink.home }
          ].map((item, index) => {
            return (
              <div
                style={{ margin: "0 20px", cursor: "pointer" }}
                key={"page-" + index}
                onClick={() => history.push(item.path)}
              >
                <span>{item.page}</span>
              </div>
            );
          })}
        </AppBarSection>
        <AppBarSpacer />
      </AppBar>
      <div style={{ marginTop: 80 }}>{props.children}</div>
    </>
  );
}

export default MainBar;
