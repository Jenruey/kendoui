import { Button } from "@progress/kendo-react-buttons";
import { Icon } from "@progress/kendo-react-common";
import { Badge, BadgeContainer } from "@progress/kendo-react-indicators";
import {
  AppBar,
  AppBarSection,
  AppBarSpacer,
  Avatar
} from "@progress/kendo-react-layout";
import React from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import Colors from "../constants/Colors";
import PathLink from "./PathLink";

const kendokaAvatar =
  "https://www.telerik.com/kendo-react-ui-develop/images/kendoka-react.png";

const PopUpContent = styled.div`
  position: absolute;
  top: 72px;
  right: 0;
  left: 0;
  height: 0;
  background-color: ${Colors.mainBgColor};
  overflow: hidden;
  transition: height 0.5s;
  div {
    text-align: center;
    margin: 25px 0;
  }
  div span:hover {
    cursor: pointer;
    color: ${Colors.linkHoverColor};
  }
  div:first-child {
    padding-top: 55px;
  }
  div:last-child {
    display: none;
  }
  @media only screen and (max-width: 768px) {
    ${(props) => props.show && "height: calc(100vh - 72px)"};
  }
`;

const MainBar = styled(AppBar)`
  background-color: transparent;
  margin-top: 20px;
  box-shadow: none !important;
  transition: margin-top 0.25s linear, background-color 0.5s;
  overflow: visible;

  .overflow-button {
    display: none;
  }

  @media only screen and (max-width: 768px) {
    margin-top: 0px !important;
    background-color: ${Colors.mainBgColor} !important;
    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.16) !important;
    .appbar-items,
    .k-avatar,
    .k-appbar-separator {
      display: none;
    }
    .overflow-button {
      display: block;
    }
  }

  @media only screen and (max-width: 600px) {
    .user-actions {
      display: none;
    }
    ${PopUpContent} div:last-child {
      display: block;
    }
  }
`;

const Menu = styled(AppBarSection)`
  ul {
    font-size: 14px;
    list-style-type: none;
    padding: 0;
    margin: 0;
    display: flex;
  }
  li {
    margin: 0 10px;
  }
  li:hover {
    cursor: pointer;
    color: ${Colors.linkHoverColor};
  }
`;

const OverflowButton = styled(Button)`
  height: 38px;
  .top,
  .bottom,
  .middle {
    background: #6c757d;
    border: none;
    height: 2px;
    width: 16px;
    position: absolute;
    left: 0;
    transition: all 0.35s ease;
    border-radius: 20px;
  }
  .top {
    top: 12px;
    ${(props) =>
      props.toggle && "transform: translateY(7px) translateX(0) rotate(45deg)"};
  }
  .bottom {
    top: 24px;
    ${(props) =>
      props.toggle &&
      "transform: translateY(-5px) translateX(0) rotate(-45deg)"};
  }
  .middle {
    top: 18px;
    ${(props) => props.toggle && "opacity: 0"};
  }
  :hover {
    .top,
    .bottom,
    .middle {
      background: black;
    }
  }
`;

const MainPages = [
  { page: "What's New", path: PathLink.home },
  { page: "About", path: PathLink.home },
  { page: "Sign in", path: PathLink.login }
];

function NavBar() {
  const [show, setShow] = React.useState(false);
  const history = useHistory();

  const handleClick = () => {
    setShow(!show);
  };

  const scrollListener = () => {
    // this setting won't work when window.innerWidth <= 768px
    // please refer to MainBar component to check !important attribute
    let appbar = document.getElementById("App-header");
    if (document.documentElement.scrollTop < 15) {
      appbar.style.marginTop = "20px";
      appbar.style.backgroundColor = "transparent";
    } else {
      appbar.style.marginTop = "0px";
      appbar.style.backgroundColor = Colors.mainBgColor;
    }
  };

  React.useEffect(() => {
    window.addEventListener("scroll", scrollListener, false);
    return function unmout() {
      window.removeEventListener("scroll", scrollListener, false);
    };
  }, []);

  const historyPush = (path) => {
    if (show) setShow(!show);
    history.push(path);
  };

  return (
    <MainBar id={"App-header"} positionMode={"fixed"}>
      <AppBarSection>
        <h1>Kendo</h1>
      </AppBarSection>
      <AppBarSpacer />
      <Menu className={"appbar-items"}>
        <ul>
          {MainPages.map((item, index) => {
            return (
              <li key={"page-" + index} onClick={() => historyPush(item.path)}>
                <span>{item.page}</span>
              </li>
            );
          })}
        </ul>
      </Menu>

      <AppBarSection className="user-actions">
        <Button look={"clear"}>
          <BadgeContainer>
            <Icon name={"bell"} />
            <Badge
              shape="dot"
              themeColor="info"
              size="small"
              position="inside"
            />
          </BadgeContainer>
        </Button>
        <span className="k-appbar-separator" />
      </AppBarSection>

      <AppBarSection>
        <OverflowButton
          look={"clear"}
          className={"overflow-button"}
          onClick={handleClick}
          toggle={show}
        >
          <span className={"top"}></span>
          <span className={"middle"}></span>
          <span className={"bottom"}></span>
        </OverflowButton>
        <Avatar shape="circle" type="image">
          <img src={kendokaAvatar} alt={"kendokaAvatar"} />
        </Avatar>
      </AppBarSection>
      <PopUpContent show={show}>
        {MainPages.map((item, index) => {
          return (
            <div
              key={"overflow-page-" + index}
              onClick={() => historyPush(item.path)}
            >
              <span>{item.page}</span>
            </div>
          );
        })}
        <div>
          <span>My Profile</span>
        </div>
        <div>
          <span>Notifications</span>
        </div>
      </PopUpContent>
    </MainBar>
  );
}

export default NavBar;
